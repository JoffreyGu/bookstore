package net.tncy.jgu.bookmarket.data;

public class InventoryEntry {
	private Book book;
	private int quantity;
	private float currentPrice;
	
	public InventoryEntry(Book book, int quantity, float currentPrice) {
		super();
		this.book = book;
		this.quantity = quantity;
		this.currentPrice = currentPrice;
	}

	public Book getBook() {
		return book;
	}

	public int getQuantity() {
		return quantity;
	}

	public float getCurrentPrice() {
		return currentPrice;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setCurrentPrice(float currentPrice) {
		this.currentPrice = currentPrice;
	}
	
}
