package net.tncy.jgu.bookmarket.data;

import java.util.ArrayList;
import java.util.List;

public class Bookstore {
	private List<InventoryEntry> inventoryEntry;

	public Bookstore() {
		super();
		this.inventoryEntry = new ArrayList<InventoryEntry>();
	}

	public List<InventoryEntry> getEtagere() {
		return inventoryEntry;
	}
	public void addBook(InventoryEntry e) {
		this.inventoryEntry.add(e);
	}
	public void removeBook(InventoryEntry e) {
		this.inventoryEntry.remove(e);
	}
	
	
}
