package net.tncy.jgu.bookmarket.data;

import net.tncy.jgu.validator.ISBN;

public class Book {
	private String title;
	private int nbPages;
	@ISBN
	private String isbn;

	public Book(String title, int nbPages) {
		super();
		this.title = title;
		this.nbPages = nbPages;
	}

	public String gettitle() {
		return title;
	}

	public void settitle(String title) {
		this.title = title;
	}

	public int getNbPages() {
		return nbPages;
	}

	public void setNbPages(int nbPages) {
		this.nbPages = nbPages;
	}
	
}
